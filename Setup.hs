import Distribution.Simple
import Distribution.PackageDescription (emptyHookedBuildInfo)
import System.Process (system)
import System.Directory (getModificationTime, doesFileExist, removeDirectory)
import Control.Monad (when, void)

main = defaultMainWithHooks $ simpleUserHooks {
  preBuild = preBuildHook,
  postClean = cleanProtoFiles
  }

isChanged :: String -> String -> IO Bool
isChanged indir filename = do
  exist <- doesFileExist buildTimeFile
  if exist then compare else create
    where
      buildTimeFile = "dist/lastbuild." ++ filename
      touch = writeFile buildTimeFile ""
      compare = do
        fileTime <- getModificationTime $ indir ++ "/" ++ filename
        buildTime <- getModificationTime buildTimeFile
        touch
        return $ fileTime > buildTime
      create = touch >> (return True)

preBuildHook _ _ = do
  generateProto

generateProto = do
  putStrLn "Generate ProtoBuf files"
  genProto "etc" "src" "CHMessages.proto"
  genProto "etc" "src" "CHModelMessages.proto"
  genProto "etc" "src" "Messages.proto"
  genProto "etc" "src" "CommonModelMessages.proto"
  genProto "etc" "src" "ModelMessages.proto"
  return emptyHookedBuildInfo

genProto indir outdir filename = do
  changed <- isChanged indir filename
  when changed $ void $ system $ "hprotoc -p Proto -d "
    ++ outdir ++ " -I " ++ indir ++ " " ++ filename

cleanProtoFiles _ _ _ _ = do
  void $ system "rm -r src/Proto"
