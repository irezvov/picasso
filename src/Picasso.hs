{-# LANGUAGE OverloadedStrings #-}
module Main where

import Web.Scotty
import Control.Applicative
import System.Environment (getArgs)
import Data.Maybe (catMaybes)
import Control.Concurrent (forkIO, threadDelay)
import System.Log (Priority(..))
import qualified System.Log.Logger as L

import Picasso.Types
import Picasso.Connection
import Picasso.Session
import Picasso.Messages.Message
import Picasso.Messages.StrategyProviderDealListRes

main = do
  [login, password] <- getArgs
  L.updateGlobalLogger "Picasso.Messages" $ L.setLevel DEBUG
  L.updateGlobalLogger "Picasso.Connection" $ L.setLevel INFO
  L.updateGlobalLogger "Picasso.Session" $ L.setLevel DEBUG
  conn <- connect "demo-hub-dev.p.ctrader.com" "5031"
  sess <- session conn
  res <- auth sess login password
  case res of
    Nothing -> putStrLn "Auth error"
    Just _ -> do
      Just providers <- getStrategyProviders sess
      deals <- flip mapM providers $ getDeals sess
      print $ map (catMaybes . map dCloseDetails) $ catMaybes deals
  scotty 3000 $ do
    get "/log" $ do
      html "<h1>Log soon will be here!</h1>"
