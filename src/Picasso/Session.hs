{-# LANGUAGE MultiParamTypeClasses #-}
module Picasso.Session (
  Login,
  Password,
  Session,
  session,
  auth,
  getStrategyProviders,
  getDeals
  ) where

import qualified Data.Map as Map
import Data.Maybe (isJust, fromJust)
import Control.Applicative ((<$>))
import Control.Monad (forever)
import Control.Monad.State
import Control.Concurrent (threadDelay)
import qualified System.Log.Logger as L
import Data.Time.Clock (diffUTCTime, getCurrentTime)

import Concurrency.OTP.Process
import Concurrency.OTP.GenServer

import Picasso.Types
import Picasso.Connection
import Picasso.Messages.Message
import Picasso.Messages.PingReq
import Picasso.Messages.PingRes
import Picasso.Messages.AuthCtidRes
import Picasso.Messages.ErrorRes
import Picasso.Messages.StrategyProviderSearchRes
import Picasso.Messages.StrategyProviderDealListRes

data Session = Session (GenServer Req RMessage)

type Login = String
type Password = String

data SessionState = SessionState {
    sRequests :: Map.Map MsgId RequestId,
    sConn :: Connection,
    sPing :: Bool,
    sMsgCnt :: MsgId
  }

initState :: Connection -> SessionState
initState conn = SessionState {
    sRequests = Map.empty,
    sConn = conn,
    sPing = False,
    sMsgCnt = 1
  }

data Req = Send SMessage
         | Recv (MsgId, RMessage)

instance GenServerState Req RMessage SessionState where
  handle_call (Send msg) req = do
    conn <- gets sConn
    mid <- gets sMsgCnt
    liftIO $ sendMessage conn $ encodeMessage msg mid
    modify $ \st -> st {
      sRequests = Map.insert mid req $ sRequests st,
      sMsgCnt = mid + 1
    }
    return noreply

  handle_cast (Recv (mid, msg)) = do
    req <- Map.lookup mid <$> gets sRequests
    when (isJust req) $ do
      replyWith (fromJust req) msg
      modify $ \st -> st { 
        sRequests = Map.delete mid $ sRequests st
      }
    return noreply

startSessionServer :: Connection -> IO (GenServer Req RMessage)
startSessionServer conn = do
  Ok gs <- start $ return $ initState conn
  return gs

session :: Connection -> IO Session
session conn = do
  ss <- startSessionServer conn
  spawn $ liftIO $ receiveLoop conn ss
  spawn $ liftIO $ ping ss
  return $ Session ss

pingInterval :: Int
pingInterval = 5 * 1000

ping :: GenServer Req RMessage -> IO ()
ping gs = do
  req@(PingReqMsg (PingReq time)) <- pingReq
  L.debugM "Picasso.Connection" $ "Ping at " ++ show time
  res <- callWithTimeout gs (Just pingInterval) $ Send $ req
  case res of
    Just (PingResMsg (PingRes time')) -> do
      L.debugM "Picasso.Connection" $ "Pong at " ++ show time'
      diff <- round . (*1000) . flip diffUTCTime time <$> getCurrentTime
      threadDelay $ (pingInterval - diff) * 1000
      ping gs
    Nothing -> do
      L.errorM "Picasso.Connection" "Ping timeout expired"
      error "Connection has been lost"

receiveLoop :: Connection -> GenServer Req RMessage -> IO ()
receiveLoop conn serv = forever $ do
  decoded <- decodeMessage <$> recvMessage conn
  case decoded of
    Just (Just mid, msg) -> cast serv $ Recv (mid, msg)
    _ -> L.warningM "Picasso.Connection" "Unknown message"
    
auth :: Session -> Login -> Password -> IO (Maybe AuthCtidRes)
auth (Session gs) log pass = do
  L.infoM "Picasso.Session" "Send auth request"
  res <- call gs $ Send $ authCtidReq log pass
  case res of
    AuthCtidResMsg msg -> return $ Just msg
    ErrorResMsg _ -> return Nothing
    _ -> error "Unexpected message"

getStrategyProviders :: Session -> IO (Maybe [ProviderId])
getStrategyProviders (Session gs) = do
  L.debugM "Picasso.Session" "Get providers list"
  res <- call gs $ Send strategyProviderSearchReq
  case res of
    StrategyProviderSearchResMsg msg -> return $ Just $ map spId $ spProviders msg
    ErrorResMsg _ -> return Nothing
    _ -> error "Unexpected message"

getDeals :: Session -> ProviderId -> IO (Maybe [Deal])
getDeals (Session gs) pid = do
  L.debugM "Picasso.Session" $ "Get deals list for " ++ show pid
  res <- call gs $ Send $ fullDealList pid
  case res of
    ErrorResMsg (ErrorRes _code _desc) -> return Nothing
    StrategyProviderDealListResMsg (StrategyProviderDealListRes deals) ->
      return $ Just deals
    _ -> error "Unexpected message"
