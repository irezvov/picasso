-- | SSL-connection with lenght-prefixied messages
module Picasso.Connection (
  Address,
  Port,
  Connection,
  connect,
  recvMessage,
  sendMessage
  ) where

import Control.Applicative ((<$>))
import Control.Monad (when)
import qualified Data.ByteString.Lazy as BS
import System.IO (IOMode(..), Handle)
import qualified Network.Socket as Sock
import qualified Data.Binary.Get as Get
import qualified Data.Binary.Put as Put
import OpenSSL (withOpenSSL)
import qualified OpenSSL.Session as SSL
import qualified System.Log.Logger as L

import Picasso.Types
import Picasso.Messages.Message

import Numeric (showHex)

type Address = String
type Port = String

data Connection = Connection SSL.SSL

prettyPrint :: BS.ByteString -> String
prettyPrint = concat . map toHex . BS.unpack
  where toHex n = let s = flip showHex " " n
                  in "0x" ++ (if length s == 2 then '0' : s else s)

getAddressInfo :: Address -> Port -> IO Sock.AddrInfo
getAddressInfo addr port =
  let hint = Sock.defaultHints { Sock.addrSocketType = Sock.Stream }
  in head <$> Sock.getAddrInfo (Just hint) (Just addr) (Just port)

-- | Create SSL-connection
connect :: Address -> Port -> IO Connection
connect addr port = withOpenSSL $ do
  addrInfo <- getAddressInfo addr port
  L.infoM "Picasso.Connection" $ "Connect to: " ++ show (Sock.addrAddress addrInfo)
  sock <- Sock.socket
            (Sock.addrFamily addrInfo)
            (Sock.addrSocketType addrInfo)
            (Sock.addrProtocol addrInfo)
  Sock.connect sock $ Sock.addrAddress addrInfo
  ctx <- SSL.context
  SSL.contextSetCiphers ctx "DEFAULT"
  SSL.contextSetVerificationMode ctx SSL.VerifyNone
  ssl <- SSL.connection ctx sock
  SSL.connect ssl
  return $ Connection ssl

atomicReceive :: SSL.SSL -> Int -> IO BS.ByteString
atomicReceive ssl n = do
  bin <- BS.fromStrict <$> SSL.read ssl n
  let len = fromIntegral $ BS.length bin
  if  len < n
    then BS.append bin <$> atomicReceive ssl (n - len)
    else return bin

-- | Recieve message prefixied by 4-length bytes
recvMessage :: Connection -> IO BS.ByteString
recvMessage conn@(Connection ctx) = do
  len <- atomicReceive ctx 4
  when (BS.null len) $ error "Connection has been closed"
  let msgLen = fromIntegral $ Get.runGet Get.getWord32be len
  msg <- atomicReceive ctx msgLen
  L.debugM "Picasso.Connection"
    $ "Recieved msg(len: " ++ show msgLen ++ "): " ++ prettyPrint msg
  return msg

-- | Add length of message to begin and send it to socket.
sendMessage :: Connection -> BS.ByteString -> IO ()
sendMessage (Connection ssl) msgBin =
  let len = Put.runPut $ Put.putWord32be $ fromIntegral $ BS.length msgBin
      rawMsg = BS.append len msgBin
  in do
    L.debugM "Picasso.Connection" $ "Send message: " ++ prettyPrint rawMsg
    SSL.write ssl $ BS.toStrict rawMsg
