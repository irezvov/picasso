module Picasso.Messages.Message where

import qualified Data.ByteString.Lazy as BS
import Data.Maybe (fromMaybe)
import Control.Applicative ((<$>), (<*>))
import Text.ProtocolBuffers.Basic (
    uToString, uFromString, defaultValue
  )
import Text.ProtocolBuffers.WireMessage (messagePut, messageGet)

import Picasso.Types
import Picasso.Messages.PingReq
import Picasso.Messages.PingRes
import Picasso.Messages.AuthCtidReq
import Picasso.Messages.AuthCtidRes
import Picasso.Messages.ErrorRes
import Picasso.Messages.StrategyProviderSearchReq
import Picasso.Messages.StrategyProviderSearchRes
import Picasso.Messages.StrategyProviderDealListReq
import Picasso.Messages.StrategyProviderDealListRes

import qualified Proto.CHContainerModelMessages.ProtoCHPayloadType as T
import qualified Proto.ContainerMessages.ProtoMessage as M
import qualified Proto.ContainerModelMessages.ProtoPayloadType as MT

type MsgId = Int

data SMessage = AuthCtidReqMsg AuthCtidReq
              | PingReqMsg PingReq
              | StrategyProviderSearchReqMsg StrategyProviderSearchReq
              | StrategyProviderDealListReqMsg StrategyProviderDealListReq

data RMessage = PingResMsg PingRes
              | AuthCtidResMsg AuthCtidRes
              | StrategyProviderSearchResMsg StrategyProviderSearchRes
              | StrategyProviderDealListResMsg StrategyProviderDealListRes
              | ErrorResMsg ErrorRes
              deriving (Show, Eq)

authCtidReq :: String -> String -> SMessage
authCtidReq login pass = AuthCtidReqMsg $ AuthCtidReq login pass

pingReq :: IO SMessage
pingReq = PingReqMsg <$> makePingReq

strategyProviderSearchReq :: SMessage
strategyProviderSearchReq = StrategyProviderSearchReqMsg StrategyProviderSearchReq

fullDealList :: ProviderId -> SMessage
fullDealList pid =
  let msg = StrategyProviderDealListReq pid Nothing Nothing Nothing
  in StrategyProviderDealListReqMsg msg

packMessage' :: Int -> BS.ByteString -> MsgId -> BS.ByteString
packMessage' payloadType msgBody mId = messagePut $ defaultValue {
    M.payloadType = fromIntegral payloadType,
    M.payload = Just msgBody,
    M.clientMsgId = Just $ uFromString $ show mId
  }

packMessage :: T.ProtoCHPayloadType 
            -> BS.ByteString
            -> MsgId
            -> BS.ByteString
packMessage payloadType = packMessage' $ fromEnum payloadType

unpackMessage :: BS.ByteString -> Int -> Maybe RMessage
unpackMessage body pt
  | pt == fromEnum MT.PING_RES = PingResMsg <$> unpack body
  | pt == fromEnum MT.ERROR_RES = ErrorResMsg <$> unpack body
  | otherwise = case toEnum pt of
    T.CH_AUTH_CTID_RES -> AuthCtidResMsg <$> unpack body
    T.CH_STRATEGY_PROVIDER_SEARCH_RES ->
      StrategyProviderSearchResMsg <$> unpack body
    T.CH_STRATEGY_PROVIDER_DEAL_LIST_RES ->
      StrategyProviderDealListResMsg <$> unpack body
    _ -> Nothing

encodeMessage :: SMessage -> MsgId -> BS.ByteString
encodeMessage (AuthCtidReqMsg msg) = 
  packMessage T.CH_AUTH_CTID_REQ $ pack msg
encodeMessage (StrategyProviderSearchReqMsg msg) = 
  packMessage T.CH_STRATEGY_PROVIDER_SEARCH_REQ $ pack msg
encodeMessage (StrategyProviderDealListReqMsg msg) = 
  packMessage T.CH_STRATEGY_PROVIDER_DEAL_LIST_REQ $ pack msg
encodeMessage (PingReqMsg msg) =
  packMessage' (fromEnum MT.PING_REQ) $ pack msg

decodeMessage :: BS.ByteString -> Maybe (Maybe MsgId, RMessage)
decodeMessage bin = case messageGet bin of
  Left _ -> Nothing
  Right (msg, _rest) ->
    let body = fromMaybe BS.empty $ M.payload msg
        mid = read . uToString <$> M.clientMsgId msg
        msg' = unpackMessage body $ fromIntegral $ M.payloadType msg
    in (,) mid <$> msg'
