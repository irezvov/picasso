module Picasso.Messages.StrategyProviderSearchReq (
  StrategyProviderSearchReq(..)
  ) where

import Text.ProtocolBuffers.Basic (
    uFromString, uToString, defaultValue
  )
import Text.ProtocolBuffers.WireMessage (messagePut, messageGet)

import Picasso.Types

import qualified Proto.CHContainerMessages.ProtoCHStrategyProviderSearchReq as P

-- incomplete implementation
data StrategyProviderSearchReq = StrategyProviderSearchReq

instance Sendable StrategyProviderSearchReq where
  pack _ = messagePut (defaultValue :: P.ProtoCHStrategyProviderSearchReq)
