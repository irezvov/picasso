module Picasso.Messages.AuthCtidReq (
  AuthCtidReq(..)
  ) where

import Text.ProtocolBuffers.Basic (
    uFromString, uToString, defaultValue
  )
import Text.ProtocolBuffers.WireMessage (messagePut, messageGet)

import Picasso.Types

import qualified Proto.CHContainerMessages.ProtoCHAuthCtidReq as P

data AuthCtidReq = AuthCtidReq {
    login :: String,
    password :: String
  }

instance Sendable AuthCtidReq where
  pack (AuthCtidReq login pass) = messagePut $ defaultValue {
      P.login = uFromString login,
      P.password = uFromString pass
    }
