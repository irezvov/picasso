module Picasso.Messages.PingReq (
  PingReq(..),
  makePingReq
  ) where

import Control.Applicative ((<$>))
import Data.Time.Clock 
import Data.Time.Clock.POSIX
import Data.Word

import Text.ProtocolBuffers.Basic (
    uFromString, uToString, defaultValue
  )
import Text.ProtocolBuffers.WireMessage (messagePut, messageGet)

import Picasso.Types

import qualified Proto.ContainerMessages.ProtoPingReq as P

data PingReq = PingReq {
    timestamp :: UTCTime
  }

makePingReq :: IO PingReq
makePingReq = PingReq <$> getCurrentTime

timestampToUtc :: Word64 -> UTCTime
timestampToUtc = posixSecondsToUTCTime . fromRational . (/1000) . fromIntegral 

utcToTimestamp :: UTCTime -> Word64
utcToTimestamp = round . (* 1000) . utcTimeToPOSIXSeconds

instance Sendable PingReq where
  pack (PingReq time) = messagePut $ defaultValue { P.timestamp = utcToTimestamp time }
