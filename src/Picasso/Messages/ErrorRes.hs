module Picasso.Messages.ErrorRes (
  ErrorRes(..)
  ) where

import Control.Applicative ((<$>))
import Text.ProtocolBuffers.Basic (
    uToString, uFromString, defaultValue
  )
import Text.ProtocolBuffers.WireMessage (messagePut, messageGet)

import Picasso.Types

import qualified Proto.ContainerMessages.ProtoErrorRes as P

data ErrorRes = ErrorRes {
    erCode :: String,
    erDescription :: Maybe String
  } deriving (Show, Eq)

instance Receivable ErrorRes where
  unpack bin = case messageGet bin of
    Left _ -> Nothing
    Right (msg, _rest) -> Just $ ErrorRes {
        erCode = uToString $ P.errorCode msg,
        erDescription = uToString <$> P.description msg
      }
