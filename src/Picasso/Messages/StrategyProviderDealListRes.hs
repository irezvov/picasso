module Picasso.Messages.StrategyProviderDealListRes (
  StrategyProviderDealListRes(..),
  Deal(..),
  TradeSide(..)
  ) where

import Data.Foldable (toList)
import Data.Maybe (fromMaybe)
import Control.Applicative ((<$>))
import Text.ProtocolBuffers.Basic (
    uFromString, uToString, defaultValue
  )
import Text.ProtocolBuffers.WireMessage (messagePut, messageGet)

import Picasso.Types

import qualified Proto.CHContainerMessages.ProtoCHStrategyProviderDealListRes as P
import qualified Proto.CHContainerModelMessages.ProtoCHDeal as D
import qualified Proto.CHContainerModelMessages.ProtoCHPositionCloseDetails as CD
import qualified Proto.CommonContainerModelMessages.ProtoTradeSide as TS

type TradeSide = TS.ProtoTradeSide

-- incomplete description
data Deal = Deal {
  dId :: DealId,
  dPositionId :: PositionId,
  dOrderId :: OrderId,
  dTradeSide :: TradeSide,
  dVolume :: Integer,
  dFilledVolume :: Integer,
  dSymbolName :: String,
  dCloseDetails :: Maybe PositionCloseDetails
  } deriving (Show, Eq)

data PositionCloseDetails = PositionCloseDetails {
  pEntryPrice :: Double,
  pProfit :: Integer,
  pSwap :: Integer,
  pCommission :: Integer,
  pBalance :: Integer,
  pClosedVolume :: Integer,
  pClosedByStopOut :: Bool,
  pComment :: Maybe String,
  pStopLossPrice :: Maybe Double,
  pTakeProfitPrice :: Maybe Double,
  pQuoteToDepositConversionRate :: Maybe Double,
  pProfitInPips :: Maybe Double
  } deriving (Show, Eq)

protoToDeal :: D.ProtoCHDeal -> Deal
protoToDeal d = Deal {
  dId = fromIntegral $ D.dealId d,
  dPositionId = fromIntegral $ D.positionId d,
  dOrderId = fromIntegral $ D.orderId d,
  dTradeSide = D.tradeSide d,
  dVolume = fromIntegral $ D.volume d,
  dFilledVolume = fromIntegral $ D.filledVolume d,
  dSymbolName = uToString $ D.symbolName d,
  dCloseDetails = protoToCloseDetails <$> D.positionCloseDetails d
  }

protoToCloseDetails :: CD.ProtoCHPositionCloseDetails -> PositionCloseDetails
protoToCloseDetails cd = PositionCloseDetails {
    pEntryPrice = CD.entryPrice cd,
    pProfit = fromIntegral $ CD.profit cd,
    pSwap = fromIntegral $ CD.swap cd,
    pCommission = fromIntegral $ CD.commission cd,
    pBalance = fromIntegral $ CD.balance cd,
    pClosedVolume = fromIntegral $ CD.closedVolume cd,
    pClosedByStopOut = CD.closedByStopOut cd,
    pComment = uToString <$> CD.comment cd,
    pStopLossPrice = CD.stopLossPrice cd,
    pTakeProfitPrice = CD.takeProfitPrice cd,
    pQuoteToDepositConversionRate = CD.quoteToDepositConversionRate cd,
    pProfitInPips = CD.profitInPips cd
  }

data StrategyProviderDealListRes = StrategyProviderDealListRes {
  spDeals :: [Deal]
  } deriving (Show, Eq)

instance Receivable StrategyProviderDealListRes where
  unpack bin = case messageGet bin of
    Left _ -> Nothing
    Right (P.ProtoCHStrategyProviderDealListRes _ deals _, _rest) ->
      Just $ StrategyProviderDealListRes $ map protoToDeal $ toList deals
