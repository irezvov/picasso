module Picasso.Messages.PingRes (
  PingRes(..)
  ) where

import Data.Time.Clock 
import Data.Time.Clock.POSIX
import Data.Time.LocalTime
import Data.Word

import Text.ProtocolBuffers.WireMessage (messagePut, messageGet)

import Picasso.Types

import qualified Proto.ContainerMessages.ProtoPingRes as P

data PingRes = PingRes {
    timestamp :: UTCTime
  }
  deriving (Show, Eq)

timestampToUtc :: Word64 -> UTCTime
timestampToUtc = posixSecondsToUTCTime . fromRational . (/1000) . fromIntegral 

instance Receivable PingRes where
  unpack bin = case messageGet bin of
    Left _ -> Nothing
    Right (msg, _rest) -> Just $ PingRes $ timestampToUtc $ P.timestamp msg
