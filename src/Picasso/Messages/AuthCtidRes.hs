module Picasso.Messages.AuthCtidRes (
  AuthCtidRes(..)
  ) where

import Control.Applicative ((<$>))
import Text.ProtocolBuffers.Basic (
    uFromString, uToString, defaultValue
  )
import Text.ProtocolBuffers.WireMessage (messagePut, messageGet)

import Picasso.Types

import qualified Proto.CHContainerMessages.ProtoCHAuthCtidRes as P
import qualified Proto.CHContainerModelMessages.ProtoCHCtidProfile as PP

data AuthCtidRes = AuthCtidRes {
    userId :: Integer,
    email :: String,
    nickname :: String,
    avatarUrl :: Maybe String
  } deriving (Show, Eq)

instance Receivable AuthCtidRes where
  unpack bin = case messageGet bin of
    Left _ -> Nothing
    Right (P.ProtoCHAuthCtidRes _ prof, _rest) -> Just $ AuthCtidRes {
        userId = fromIntegral $ PP.userId prof,
        email = uToString $ PP.email prof,
        nickname = uToString $ PP.nickname prof,
        avatarUrl = uToString <$> PP.avatarUrl prof
      }
