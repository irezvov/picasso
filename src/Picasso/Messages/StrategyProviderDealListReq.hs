module Picasso.Messages.StrategyProviderDealListReq (
  StrategyProviderDealListReq(..)
  ) where

import Control.Applicative ((<$>))
import Data.Time.Clock 
import Data.Time.Clock.POSIX
import Data.Word

import Text.ProtocolBuffers.Basic (
    uFromString, uToString, defaultValue
  )
import Text.ProtocolBuffers.WireMessage (messagePut, messageGet)

import Picasso.Types

import qualified Proto.CHContainerMessages.ProtoCHStrategyProviderDealListReq as P

-- incomplete implementation
data StrategyProviderDealListReq = StrategyProviderDealListReq {
    drStrategyProviderId :: ProviderId,
    drLimit :: Maybe Integer,
    drFrom :: Maybe UTCTime,
    drTo :: Maybe UTCTime
  }

instance Sendable StrategyProviderDealListReq where
  pack msg = messagePut $ defaultValue {
    P.strategyProviderId = fromIntegral $ drStrategyProviderId msg,
    P.limit = fromIntegral <$> drLimit msg,
    P.fromTimestamp = toTimestamp <$> drFrom msg,
    P.toTimestamp = toTimestamp <$> drTo msg
    }
    where toTimestamp = round . (* 1000) . utcTimeToPOSIXSeconds
