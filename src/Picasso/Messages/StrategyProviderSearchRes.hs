module Picasso.Messages.StrategyProviderSearchRes (
  StrategyProviderSearchRes(..),
  StrategyProvider(..),
  TradingStyle(..)
  ) where

import Data.Foldable (toList)
import Data.Maybe (fromMaybe)
import Control.Applicative ((<$>))
import Text.ProtocolBuffers.Basic (
    uFromString, uToString, defaultValue
  )
import Text.ProtocolBuffers.WireMessage (messagePut, messageGet)

import Picasso.Types

import qualified Proto.CHContainerMessages.ProtoCHStrategyProviderSearchRes as P
import qualified Proto.CHContainerModelMessages.ProtoCHStrategyProvider as SP
import qualified Proto.CHContainerModelMessages.ProtoCHTradingStyle as TS

type TradingStyle = TS.ProtoCHTradingStyle

data StrategyProvider = StrategyProvider {
    spId :: ProviderId,
    spCtidTraderAccountId :: Integer,
    spName :: String,
    spCommissionPerMillion :: Integer,
    spTradingStyle :: [TradingStyle],
    spTraderNickname :: Maybe String,
    spAvatarUrl :: Maybe String,
    spDeleted :: Bool,
    spRoi :: Maybe Double,
    spBalanceInUSD :: Maybe Integer,
    spFollowersCount :: Maybe Integer,
    spActiveFollowersCount :: Maybe Integer,
    spAllTimeActiveFollowersCount :: Maybe Integer,
    spMirroringFundsInUSD :: Maybe Integer
  } deriving (Eq, Show)
  
protoToProvider :: SP.ProtoCHStrategyProvider -> StrategyProvider
protoToProvider p = StrategyProvider {
    spId = fromIntegral $ SP.strategyProviderId p,
    spCtidTraderAccountId = fromIntegral $ SP.ctidTraderAccountId p,
    spName = uToString $ SP.name p,
    spCommissionPerMillion = fromIntegral $ SP.commissionPerMillion p,
    spTradingStyle = toList $ SP.tradingStyle p,
    spTraderNickname = uToString <$> SP.traderNickname p,
    spAvatarUrl = uToString <$> SP.avatarUrl p,
    spDeleted = fromMaybe False $ SP.deleted p,
    spRoi = SP.roi p,
    spBalanceInUSD = fromIntegral <$> SP.balanceInUsd p,
    spFollowersCount = fromIntegral <$> SP.followersCount p,
    spActiveFollowersCount = fromIntegral <$> SP.activeFollowersCount p,
    spAllTimeActiveFollowersCount = fromIntegral <$> SP.allTimeActiveFollowersCount p,
    spMirroringFundsInUSD = fromIntegral <$> SP.mirroringFundsInUsd p
  }

data StrategyProviderSearchRes = StrategyProviderSearchRes {
    spProviders :: [StrategyProvider]
  } deriving (Show, Eq)

instance Receivable StrategyProviderSearchRes where
  unpack bin = case messageGet bin of
    Left _ -> Nothing
    Right (P.ProtoCHStrategyProviderSearchRes _ provs, _rest) ->
      Just $ StrategyProviderSearchRes $ map protoToProvider $ toList provs
