{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Picasso.Types where

import qualified Data.ByteString.Lazy as BS

class Sendable a where
  pack :: a -> BS.ByteString

class Receivable a where
  unpack :: BS.ByteString -> Maybe a

newtype ProviderId = ProviderId Integer deriving (Show, Eq, Ord, Num, Real, Enum, Integral)

newtype DealId = DealId Integer deriving (Show, Eq, Ord, Num, Real, Enum, Integral)

newtype PositionId = PositionId Integer deriving (Show, Eq, Ord, Num, Real, Enum, Integral)

newtype OrderId = OrderId Integer deriving (Show, Eq, Ord, Num, Real, Enum, Integral)
